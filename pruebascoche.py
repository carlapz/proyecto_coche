from entregacoche import Coche
import unittest

class TestCoche (unittest.TestCase):
    def test_acelerar (self):
        coche01= Coche('Negro', 'Mercedes', 'GLC', '2024DPL', 80)
        acelerar = coche01.Acelerar()
        self.assertEqual(acelerar,120)

    def test_frenar (self):
        coche01= Coche ('Negro', 'Mercedes', 'GLC', '2024DPL', 80)
        frenar = coche01.Frenar()
        self.assertEqual(frenar,75)

    
if __name__ == '__main__':
    unittest.main()
    
    

