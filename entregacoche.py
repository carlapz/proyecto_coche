class Coche: 
    def __init__ (self, color, marca, modelo, matricula, velocidad): 
        self.__color = color 
        self.__marca = marca
        self.__modelo = modelo 
        self.__matricula = matricula
        self.__velocidad = velocidad 

    def Acelerar(self): 
        return self.__velocidad + 40

    def Frenar (self):
        return self.__velocidad - 5

    def ImpCoche (self):
        print("El coche es un {marca} de color {color} , modelo {modelo}, cuya matrícula es {matricula}".format (marca=self.__marca,  color=self.__color,modelo=self.__modelo, matricula= self.__matricula))



coche01= Coche ('Negro', 'Mercedes', 'GLC', '2024DPL', 80)

coche01.ImpCoche()